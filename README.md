# Overview
PrimedIO is different from black-box personalisation platforms. Most other platforms take your raw data, and return `predictions` ('*recommendations as a commodity*'). By doing so, your data and the value created from your data no longer belong to you. 

PrimedIO, in contrast, receives `predictions` and makes those available to your end-users by integrating with your customer-outlets. This means that you decide how these `predictions` are created: `predictions` can be the output of your data scientists, externally sourced, manually curated lists, one of our prepackaged Machine Learning (ML) models or a combination of the aforementioned. Either way, you remain in control of both your data ánd your predictive artifacts. 

![alt text](/images/dev-overview.png "PrimedIO overview")

By defining a generic framework for delivering `predictions` and handling the concerns about latency, security and scalability, PrimedIO enables fast ML iterations and opens up possibilities for personalisation including and beyond recommendations.

# Concepts
The core of PrimedIO is that we define sets of `signals` and sets of `targets`. The two are connected through relationships, or `predictions`. A `signal` is usually something that we deduce from a customer event or interaction: __the observed variable__. Examples of `signals` include device type, cookieId, userId, time of day, and more. `targets` then are our __dependent variables__, the things we want to predict and use to vary customer experience among our users. 

At this time, `targets` can only be discrete and need to represent an 'actionable' output on the customer-facing front-end.

Actionable means that a `target` usually does not map entirely with what a data scientist would call a __dependent variable__. Typically, a __dependent variable__ corresponds to a hypothesis ('*Alice will cancel her contract next month*', '*Bob likes Total Recall*' or '*Chris is a male between 20 and 40 years old*'). `predictions` as conventionally understood then assign probabilities to these hypotheses using some statistical artifact. These outcomes, however useful, do not represent 'actionable' `targets`: populating PrimedIO with this data will inevitably lead to adding additional logic in the front-end:

```sql
if churnProbability > 0.5 then showPopup();
```

Doing this frequently and over various platforms will make it next to impossible to systematically measure the impact of your models on KPI's. As a result front-end implementation and data science R&D style development often become unneccesarily tangled, slowing down development on both sides. Instead, `targets` map to the last bit of logic, where the model outcomes are joined with actions that impact the user: `showPopup();` in the above contrived example.

`predictions` connect the two together; for each signal there will be a `prediction` for each `target` with an associated score (usually the output of some statistical model, but sometimes this is provided by a human actor such as the product owner).

![alt text](/images/dev-signal-target-prediction.png "PrimedIO signals, targets and predictions")

Generally, there are two ways of interacting with PrimedIO: (1) mutating the set of predictions and (2) querying the set of predictions for a given campaign. The input for PrimedIO queries (or, _personalise calls_) is a set of `signals` associated with a certain event and the output (or what is returned by PrimedIO) of the personalise call will be a list of `targets` ordered by their blended, predicted relevance given those inputs.

_PrimedIO defines models as a set of relationships between an __observed variable__ and an __actionable outcome__. Each relationship, or `prediction`, represents the predicted relevance of the outcome given observed variable._

## Features and Signals
`features` represent raw event properties as logged during a user event or interaction. You can specify custom `features` or use the predefined ones:

-----------------------------------------------------------------
| name | type |  description | example |
| ---- | ---- | ------------ | ------- |
| `now` | Date object | local timestamp of the event | `Tue Jul 03 2018 07:59:46 GMT+0200 (CEST)` |
| `ua` | string | useragent string | `Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) ....` |
| `vuuid` | string | unique identifier for this pageview | `e47bf759-705d-424d-ab3c-c09979e173c9` |
| `duuid` | string | unique identifier for this device | `e47bf759-705d-424d-ab3c-c09979e173c9` |
| `url` | string | current url | `https://www.example.com/home` |
-----------------------------------------------------------------

From these `features` you derive categorical `signals` using detectors, or you provide fixed values such as 'cookieId' or 'userId'. Example 'signals' include 'deviceType' (derived from `ua`), 'cookieId' (fixed value) or 'dayOfTheWeek' (derived from `now`). 

```python
s0 = { 'key': 'iphone', 'type': 'deviceType' }
s1 = { 'key': 'alice' , 'type': 'userId' }
```

For `signals` to succesfully trigger `predictions`, they can't only be known at query time. Data scientists will need to use the same events and properties to train their models. So logging the above `features`, either using the PrimedIO tracking facilities or a third-party solution, and storing the data in a data lake is therefore necessary. Data scientists can then use the very same inputs that trigger `predictions`, to match with their models consistently.

## Targets
Actionable outcomes are represented by `targets`. A `target` in PrimedIO is represented by a 'key' and a 'value'. The key should be unique for the `target` within a `universe`. The value is a freeform dictionary and can contain anything you like: HTML content, item ID's, properties, etc.

```python
t0 = { 'key': '123', 'value': { 'html': 'alert("this popup brought to you by PrimedIO");' } }
t1 = { 'key': '456', 'value': { 'id': 'myid' } }
t2 = { 'key': '789', 'value': { 'img': 'http://stylus-lang.com/img/octocat.svg' } }
```

## Predictions
A prediction describes the predicted relevance between a given `signal` and `target` pair.

```python
prediction = { 'sk': 'iphone', 'tk': '123', 'score': 0.75 }
```

## Models

## Universes

## Experiments

## Campaigns

## A/B Variants


# Implementation
The PrimedIO engine stores predictions and meta-information such as about campaigns, A/B testing, Apikeys and the like. Generally, implementing PrimedIO involves three different stakeholders: 

1. Front-end developers: this is where the results from PrimedIO will ultimately end up, and be used to construct a personalised experience for the end-user. Typically, the front-end engineer and the data scientist sit together to determine which `signals` will be made available (e.g. 'userId', 'cookieId'), and what output `targets` shall be used. 
2. Data scientists: With the above agreed on `signals` the data scientist can train any model and map the `predictions` from `signals` to `targets`. After signing off the model using offline validation metrics the `predictions` can be uploaded to PrimedIO.
2. Data engineers: Data engineers use the data scientists' pipeline to regularly upload predictions to PrimedIO, or implement a streaming pipeline to update PrimedIO in real-time.

Additionally, the REST api and admin gui are available to facilitate specific use cases, and easier management.

## Front-end
Client-side integrations can use the JavaScript and Objective-C libraries, or resort to calling the REST api directly. All libraries expose two endpoints: `personalise` and `convert` and collect `signals` to be sent along with the calls. Optionally, the libraries can also perform real-time behavioral tracking.

### Installation
```html
<script src="https://cdn.primed.io/<VERSION>-stable/primedio.js"></script>
```
```shell
npm install primednodejs
```
```objective-c
To be added
```

### Initialization
```javascript
const pio = PRIMED({
    connectionString: "https://primed.io",
    nonce: new Date().toISOString(),
    publicKey: "xxxxxxxxxxxxxxxxxxxxxxxx",
    signature: "xxxxxxxxxxxxxxxxxxxxxxxx",
});
```
```javascript
var PRIMEDIO = require('primednodejs');

var pio = new PRIMEDIO({
    api_url: "https://primed.io",
    public_key: 'xxxxxxxxxxxxxxxxxxxxxxxx', 
    secret_key: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
});
```
```objective-c
To be added
```

In order for JavaScript client-side applications to make authorized calls, it expects two server-side generated values: `nonce` and `signature`. The signature is generated by concatenating the `publicKey`, the `secretKey` and the `nonce`. The publicKey and secretKey are provided via your PrimedIO contact. The `nonce` is the Unix timestamp in seconds from midnight 1970 UTC of the moment at which you are generating your signature. The concatenation of these three values is then sha512 hashed and the resulting value is the signature.

For the NodeJS, and Objective-C implementations this step is not needed.

```python
# SERVER-SIDE PYTHON EXAMPLE CODE
nonce = int(datetime.datetime.now(datetime.timezone.utc).timestamp())
local = hashlib.sha512()
signature = "{}{}{}".format(pubkey, secretkey, nonce)
local.update(signature.encode('utf-8'))
signature = local.hexdigest()
```

### Use
```html
<div id="$pio.frontpage.recommendations">
    <!-- 
    placeholder where successful calls will render personalised elements 
    typically, this div is not empty to begin with but contains reasonable 'fallback' content which is subsequently overwritten when the personalise calls succeed
    -->
</div>
```
```javascript
pio.detectors.bind({userId: 'alice'});

pio.campaigns.bind({
    key: 'frontpage.recommendations', // this should match div id suffix
    limit: 5,
    fn: ({ value, rank, ruuid, el, result }) => {
        // perform page rendering here
    }
});

pio.render();
```
```javascript
pio.personalise({
    campaign: 'frontpage.recommendations',
    signals: { userId: 'ALICE'},
    limit: 5,
    abvariant: 'A' // optional
}).then( value => {
    console.log(value);
    // expected output: "An array of result objects"
}).catch( reason => {
    console.error( 'something went wrong: ', reason );
});
```
```javascript
var promise = pio.personalise({
    campaign: 'frontpage.recommendations',
    signals: { 
        'userId': 'ALICE'
    },
    limit_results: 5,
    abvariant: 'A' // optional
});

promise.then(function(response) {
    // 'response' is a wrapper object,
    // calling 'response.all()' returns
    // the list of results which can then be
    // used to render the page
    var results = response.all();
}).catch(function (err) {
    console.log('Personalise failed: ' + err);
});
```
```objective-c
NSDictionary *dictSignals = @{@"userId": @"ALICE"};

[primed personalise:@"frontpage.recommendations" signals:dictSignals limit:5 success:^(NSDictionary *response) {    
    //Handle response
} failed:^(NSString *message) {
    //Handle message
}];
```

#### Detectors.bind()
Detectors provide context which is in turn used upon calling the Primed backend for personalisation calls. Each detector delivers a 'signal' value that corresponds to the signals used to train the model. A detector can be a fixed value, or a function which returns a value.

```javascript
pio.detectors.bind({
    userId: function() { return 'alice'; }
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| key | string | Yes | signal type name | `userId` |
| value | literal OR function | Yes | either a literal value or a function that returns a literal value | `alice` OR `function() { return 'alice'; }` |

#### Campaigns.bind()
For each bound campaign, primedjs will call the PrimedIO backend asynchronously retrieving the corresponding results. All signals (derived from the detectors) will be sent along with the requests.

```javascript
pio.campaigns.bind({
    key: 'frontpage.recommendations',
    limit: 5,
    fn: ({ value, rank, ruuid, el, result }) => {
        // create a new HTML node
    }
});
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| key | string | Yes | campaign key | `frontpage.recommendations` |
| limit | integer | No (defaults to 5) | number of results to return | `10` |
| fn | function | Yes | asynchronous callback function that fires for each result as it is returned by the PrimedIO backend | `fn: ({ value, rank, ruuid, el, result }) => { /* process result */ }` |

### Mark results as converted
Upon successful personalisation, a list of results will be returned. Each result will contain a unique RUUID (Result UUID). It is used to track the conversion of this particular outcome, which is used to identify which of the A/B variants performed best. Conversion is also generic, but generally we can associate this with clicking a particular outcome. In order for PrimedIO to register this feedback, another call needs to be made upon conversion. This in turn allows the system to evaluate the performance (as CTR) of the underlying Model (or blend).

```javascript
pio.convert({
    ruuid: "6d2e36d1-1b58-4fbc-bea8-868e3ec11c87",
    data: { heartbeat: 0.1 }
});
```
```javascript
pio.convert({
    ruuid: "6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"
}); 
```
```objective-c
NSDictionary *data = @{ @"heartbeat": @"0.1" };
[primed convert:@"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87" data:dictData];
```

| arg | type | required | description | example |
| --- | ---- | -------- | ------ | ------- |
| ruuid | string | Yes | ruuid for which to register conversion | `"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"` |
| data | dictionary | No | freeform data payload | `{ heartbeat: 0.1 }` |

## Data engineers
### Installation
```python
pip install pyprimed
```

### Initialization
```python
from pyprimed.pio import Pio

# instantiate library using connection uri
pio = Pio(uri='http://<user>:<password>@<api_url>:<port>')
```

# Upload semantics
All upload operations operate on two sets:  
1. the already present set in the db, let's call this `X`  
2. the new set, which is being uploaded, let's address this set as `Y`

## Delsert
```python
m.signals.delsert(new_signals)
m.targets.delsert(new_targets)
pio.predictions.on(model=model, universe=universe).delsert(new_predictions)
```

When performing an upload, using the `delsert` semantics the following occurs:
1. the client presents set `Y` to the server
2. the server verifies for each record of the union of sets `X` and `Y` which of the following applies:  
    * (`UPDATED`) the record exists in both `X` and `Y`  
    * (`DELETED`) the record only exists in `Y`  
    * (`NEW`) the record only exists in `X`  
3. the server performs corresponding actions on each of the records, depending on whether is was `UPDATED`, `DELETED` or `NEW`, which is to say:  
    * `UPDATED`: overwrite the record in `X` with the record from `Y`  
    * `DELETED`: delete the record from `X`  
    * `NEW`: add the record to `X`  

## Upsert
```python
m.signals.upsert(new_signals)
m.targets.upsert(new_targets)
pio.predictions.on(model=model, universe=universe).upsert(new_predictions)
```

When performing an upload, using the `upsert` semantics the following occurs:
1. the client presents set `Y` to the server
2. the server verifies for each record of the union of sets `X` and `Y` which of the following applies: 
    * (`UPDATED`) the record exists in both `X` and `Y`  
    * (`DELETED`) the record only exists in `Y`  
    * (`NEW`) the record only exists in `X`  
3. the server performs corresponding actions on each of the records, depending on whether is was `UPDATED`, `DELETED` or `NEW`, which is to say:  
    * `UPDATED`: overwrite the record in `X` with the record from `Y`  
    * `DELETED`: do nothing  
    * `NEW`: add the record to `X`

## Update (sync)
*ONLY SUPPORTED FOR PREDICTIONS*
```python
pio.predictions.on(model=model, universe=universe).update({"sk": "alice", "tk": "article1", "score": 0.75})
```

The update call will instantly (synchronously) update the prediction. The call only takes a single prediction to avoid scenarios where sending large bulks might congest and deadlock the system. If the prediction does not exists an exception is raised.

## Which one should I use?
If you intend to incrementally update an existing set of `signals`/`target`/`predictions`, use the `upsert` call. It will create those records not yet in the system, as well as updating those that do exist.

For a complete synchronisation use `delsert`, it will ensure that the system is completely synchronised with the set that you are sending.

Keep in mind that the above actions are asynchronous. If you desire instant updates of existing predictions, opt for the `update` call.

## PySpark
With the addition of `piospark` PrimedIO offers functionality to more readily integrate with Apache Spark. Notably, the package provides PySpark UDFs that can do the heavy lifting of converting DataFrame columns to a format that is ready for PrimedIO to receive.

```python
# df contains your predictions, we'll assume there are 
# at least the following columns:
#
#     signal_key
#     target_key
#     target_url
#     target_img
#     target_title
#     predicted_score
df

# import the piospark module
from pyprimed import piospark

# we use piospark.udf.signal() to obtain a udf that will 
# take care of creating our signals
signals = df\
    .withColumn("signal", piospark.udf.signal()('signal_key'))\
    .select("signal")

# we use piospark.udf.target() to obtain a udf that will 
# take care of creating our signals. Note that we can provide
# additional fields that will constitute the 'value' of the 
# `Target`. Keep in mind that the order of the fields as 
# specified `piospark.udf.target('img', 'url', 'title')`
# has to be consistent with the order in which you call the 
# udf `('tk', 'target_img', 'target_url', 'target_title')`
targets = df\
    .select('tk', 'target_img', 'target_url', 'target_title')\
    .distinct()\
    .withColumn("target", piospark.udf.target('img', 'url', 'title')('target_key', 'target_img', 'target_url', 'target_title'))\
    .select("target")

# we use piospark.udf.prediction() to obtain a udf that will 
# take care of creating our predictions
predictions = df\
    .withColumn("prediction", piospark.udf.prediction()('signal_key', 'target_key', 'score'))\
    .select("prediction")

# upload the signals to a model with the name 'mymodel', 
# be careful that we do a `collect()` in this example.
# Also note that the `json.loads()` is needed, as
# the PySDK expects dictionaries, and not Strings.
pio\
    .models\
    .find(name="mymodel")\
    .first\
    .signals\
    .upsert([json.loads(row.signal) for row in signals.collect()])

# upload the targets to a universe with the name 'myuniverse', 
# be careful that we do a `collect()` in this example.
# Also note that the `json.loads()` is needed, as
# the PySDK expects dictionaries, and not Strings.
pio\
    .universes\
    .find(name="myuniverse")\
    .first\
    .targets\
    .upsert([json.loads(row.target) for row in targets.collect()])

# upload the predictions to a model with the name 'mymodel', 
# be careful that we do a `collect()` in this example.
# Also note that the `json.loads()` is needed, as
# the PySDK expects dictionaries, and not Strings.
pio\
    .models\
    .find(name="mymodel")\
    .first\
    .predictions\
    .upsert([json.loads(row.prediction) for row in predictions.collect()])
```

## Quick start
### Initialize
```python
from pyprimed.pio import Pio

pio = Pio(uri='http://<user>:<password>@<api_url>:<port>')
```

### Create a Universe
```python
u = pio.universes.create(name='myfirstuniverse')
```


### Create a Model
```python
m = pio.models.create(name='myfirstmodel')
```

### Upload predictions
```python
# prepare a set of predictions
# WARNING: `sk` and `tk` should always be a string!
predictions = [
    {'sk': 'ALICE', 'tk': 'ARTICLE-1', 'score': 0.35},
    {'sk': 'BOB', 'tk': 'ARTICLE-1', 'score': 0.75}, 
    {'sk': 'CHRIS', 'tk': 'ARTICLE-1', 'score': 0.15},
    {'sk': 'ALICE', 'tk': 'ARTICLE-2', 'score': 0.25},
    {'sk': 'BOB', 'tk': 'ARTICLE-2', 'score': 0.15}, 
    {'sk': 'CHRIS', 'tk': 'ARTICLE-3', 'score': 0.43},
    {'sk': 'ALICE', 'tk': 'ARTICLE-3', 'score': 0.69},
    {'sk': 'BOB', 'tk': 'ARTICLE-3', 'score': 0.13}, 
    {'sk': 'CHRIS', 'tk': 'ARTICLE-3', 'score': 0.01}
]

# retrieve our universe
u = pio.universes.filter(name='myfirstuniverse').first

# retrieve our model
m = pio.models.filter(name='myfirstmodel').first

# upload predictions for this model and universe
pio\
    .predictions\
    .on(model=m, universe=u)\
    .upsert(predictions)
```

### Create a campaign
```python
from pyprimed.models.abvariant import CustomAbvariant, RandomControl, NullControl

# retrieve our universe
u = pio.universes.filter(name='myfirstuniverse').first

# retrieve our model
m = pio.models.filter(name='myfirstmodel').first

# create a campaign
c = u.campaigns.create(key='myfirstcampaign', name='something descriptive')

# create an experiment for which we will define abvariants
e = c.experiments.create(name='myfirstexperiment')

# we create a custom abvariant that uses a single model as well as 
# defining two control abvariants
ab0 = CustomAbvariant(label='A', models={m1: 1.0})
ab1 = RandomControlAbvariant(label='B')
ab2 = NullControlAbvariant(label='C')

# attach the abvariant to the experiment, ensuring 80% of traffic will flow to
# the abvariant, and 10% will flow to RandomControl and 10% will flow to 
# NullControl
e.abvariants.create({ab: 0.8, ab1: 0.1, ab2: 0.1})
```

### Updating Experiments
```python
# pyprimed
from pyprimed.models.abvariant import CustomAbvariant, RandomControl, NullControl

# we create a custom abvariant that blends models m1 and m2 using a 60%/40% weight ratio
ab0 = CustomAbvariant(label='A', models={m1: 0.6, m2: 0.4})
ab1 = RandomControlAbvariant(label='B')
ab2 = NullControlAbvariant(label='C')

# we attach a single abvariant to the experiment, which will take 100% of traffic
e.abvariants.create({ab: 1.0})

# updating the the abvariant (atomically) can be done as follows:
e.abvariants.update({ab: 0.8, ab1: 0.1, ab2: 0.1})
```

### Personalise
```python
u = pio.universes.filter(name="myfirstuniverse").first
c = u.campaigns.filter(key='myfirstcampaign').first

c.personalise(
  pubkey='mypubkey',
  secretkey='mysecretkey',
  signals={'userid': 'BOB'},
  limit=5,
  abvariant='A' // optional
)
```

## Advanced

### Blending
Creating a blend works by adding an `experiment` to a `campaign`, and an `abvariant` to that `experiment` with multiple associated `models`. Each of these `models` are assigned a weight (the total weight needs to sum to `1.0`):
```python
# pyprimed
from pyprimed.models.abvariant import CustomAbvariant, RandomControl, NullControl

# create models
m1 = pio.models.create(name="my_device_model")
m2 = pio.models.create(name="my_cbf")

# create campaign and blends
c = u.campaigns.create(key="dummy.frontpage.recommendations") 
e = c.experiments.create(name="myexperiment", abselectors=['device'])

# we create a custom abvariant that blends models m1 and m2 using a 60%/40% weight ratio
ab0 = CustomAbvariant(label='A', models={m1: 0.6, m2: 0.4})
ab1 = RandomControlAbvariant(label='B')
ab2 = NullControlAbvariant(label='C')

# attach the abvariants to the experiment, ensuring 80% of traffic will flow to
# the abvariant, and 10% will flow to RandomControl and 10% will flow to 
# NullControl
e.abvariants.create({ab: 0.8, ab1: 0.1, ab2: 0.1})

res_for_A = c.personalise(keys={'device': 'iphone', 'itemid': '123'}, abvariant='A')
res_for_CONTROL = c.personalise(keys={'device': 'iphone', 'itemid': '123'}, abvariant='B')
```
Upon personalisation, the blender takes input `keys`, for instance `{'userid: 'myUserId', 'device': 'iPhone, 'timeofday': 'Morning'}`, `campaign` key and an `abvariant`. The blender then matches the provided keys against the `signals` in the database, and returns the `targets` with corresponding `prediction` scores. The result set is then grouped by the `target` key, and the `prediction` scores are combined using a weighted average into the `wscore`, applying the model weights assigned previously when creating the blended `abvariant`. Each `wscore` is then optionally (if this is a `recency`-enabled `abvariant`) multiplied with the `recency`-coefficient after which the list is sorted by `fscore` in descending order. Depending on how many results are requested, the top `n` are returned to the client.

### Dithering
The goal of a recommender is to find relevant items that a user may be interested in. The list of recommended items is typically sorted by the items’ relevance scores (highest to lowest) and is presented to the user in that order.  These scores may not change much over time, meaning that the list of recommendations for individual users don’t change much either.  Depending upon your user interface, you’ll also be restricted to showing only the top n recommendations to users in a single screen at a time, despite the fact that you may have generated a much larger list of recommendations for them.  This means that when users return, many will see pretty much the same top n recommendations every visit and you’ll miss the opportunity to show them ‘less relevant’, but perhaps very interesting, recommendations.

Dithering provides a solution to this problem. The idea behind dithering is to re-order the recommendations list by adding random noise to the original relevance-based ordering. This results in surfacing some of the items that are further down the list (e.g. from the second page or even later pages) to the first page.  In doing so, it’s possible to create the illusion of freshness in your list of recommendations as they appear to change regularly between visits although they may actually have been generated from a single run of a recommender model.  What’s more, there are also benefits to recommending more items to users as it increases the variety of interactions from which your system can learn.

![dithering equation](https://buildingrecommenders.files.wordpress.com/2015/11/ditheringequation.png "Dithering Equation")

```python
# pyprimed
from pyprimed.models.abvariant import CustomAbvariant

ab = CustomAbvariant(label='A', models={m1: 1.0}, dithering=True, epsilon=10)
```

### Recency boosting
Modelling relevance as a function of time since publication can be done using the recency functionality. Each `target` can be assigned three values: `published_at` (time of `target` publication), `recency_histogram` (discretization of decay function with arbitrary bins, between 0.0 'irrelevant' and 1.0 'relevant'), `recency_xmax` (maximum amount of seconds that item remains relevant). 

Setting these values determine the relevance decay over time (meaured in seconds since publication date) using a histogram (which can be set to arbitrary resolution to support almost any decay function shape). After `recency_xmax` in seconds, the relevance coefficient will always be 0.0 (rendering the `target` 'irrelevant'). 

Upon calling personalisation, for each target the blender will calculate the time difference (`delta`) between now and `published_at` in seconds. By looking up the `delta` in the histogram (x-axis) the blender retrieves the corresponding y-value, which it returns as the relevance coefficient mentioned above. The relevance coefficient will be applied to the `wscore` right before ordering the results, ensuring the 'irrelevant' `targets` end up at the bottom of the list. 
```python
# pyprimed
from pyprimed.models.abvariant import CustomAbvariant, CONTROL

targets = [
  {
    "key": "article1", 
    "value": {"title": "my article"}, 
    "published_at": "2017-10-05T14:48:00.000Z", 
    "recency_histogram":[1.0, 0.5], 
    "recency_xmax":1800
  },
  {
    "key": "article2", 
    "value": {"title": "my next article"}, 
    "published_at": "2017-11-05T14:48:00.000Z", 
    "recency_histogram":[1.0, 0.84, 0.68, 0.52, 0.36, 0.2], 
    "recency_xmax":3600
  }
]

# create campaign. experiment and abvariant including recency
c = u.campaigns.create(name="mycampaign")
e = c.experiments.create(name="myexperiment", abselectors=['device'])
ab = CustomAbvariant(label='A', models={m1: 0.6, m2: 0.4}, recency=True) 
e.abvariants.create({ab: 1.0})
```
The above configuration will make the `recency`-coefficient decay over time for both `article1` and `article2`, but in different ways. Let's start with `article1`: within the first 15 minutes (the first out of 2 buckets: `(3800/2) = 900 seconds`) of the lifetime, the article will remain unaffected by relevance decaying over time: the first bucket is set to `1.0`. After 15 minutes, the bucket is set to `0.5` and effectively halves the `wscore`; penalizing the article for it's age. For `article1` we have set a maximum lifetime of `1800` seconds (a half hour), meaning that any `personalise` call that triggers `article1` more then half an hour after publication (set at 2017-10-05T14:48:00.000Z) will cause this article to end up at the bottom of the results.

For `article2`, the decay is much smoother and takes place over a larger timespan (1 hour). We gradually decrease the `recency`-coefficients by `0.16` for each `10` minute (`600` second) bucket.

Please note that the histogram length is arbitrary and can vary among targets, the same goes for the `recency_xmax`.

## A/B Testing
We at Primed.io believe that something that can't be measured, can't be improved. Personalisation is no exception and so Primed.io offers advanced A/B testing. This feature allows product-owners and data-scientists to define different blends of models as separate A/B variants and systematically benchmark these variants against each other. Traditionally, A/B testing (sometimes called split testing) is comparing two versions of a web page to see which one performs better. You compare two web pages by showing the two variants (let's call them `A` and `B`) to similar visitors at the same time. The one that gives a better conversion rate, wins!

In Primed.io, every `campaign` needs at least one associated `experiment` to function. This `experiment` then needs at least one `abvariant` to be associated, we could call this 'the baseline' or 'the control group'. 

### A/B membership assignment
There are three ways in which A/B membership can be managed: `FORCED`, `STICKY` and `WRANDOM`.

#### FORCED
By specifying the `Abvariant` label upon calling personalisation the system simply delivers the corresponding `Abvariant` and performs no further assignment by itself.
```python
from pyprimed.models.abvariant import ControlAbvariant, CustomAbvariant

e = mycampaign.experiments.create(name="myexperiment")
control = ControlAbvariant(label='A')
ab = CustomAbvariant(label='B', models={mymodel: 1.0})
e.abvariants.create({ab: 0.01, control: 0.99})

res1 = c.personalise(keys={'device': 'iphone', 'userid': '123'}, abvariant='A') # abvariant with label 'A' will be returned
```

#### STICKY
For this type of membership assignment, the systems ensures it is constant over time. The way it does so is by using a predefined `abselector`, to be set on the `experiment`, which is used to retrieve a `signal` and use it's corresponding value to perform the membership assignment. In order to set up the system for this type of membership assignment, perform the following:
```python
e = mycampaign.experiments.create(name="myexperiment", abselectors=['userid'])
ab = CustomAbvariant(label='A', models={mymodel: 1.0})
e.abvariants.create({ab: 0.99, CONTROL: 0.01})

res1 = c.personalise(keys={'device': 'iphone', 'userid': '123'}) # userid `123` is used to seed the abmembership assignment
res2 = c.personalise(keys={'device': 'iphone'}) # no userid signal is found, so defaulting to `WRANDOM`
```
Any calls made to the `personalise` service for this `campaign` shall now attempt to match signals with the `userid` key. If the signal is found, it uses the signal value to assign an A/B group, this assignment will be the same throughout the `experiment` as well as for that particular `userid` value. Considering the weight distribution of 0.01/0.99 we can ensure that 99% of the possible 'userids' will be assigned to the `Abvariant`, and 1% of the `userids` will be assigned to `CONTROL`. If the the signal is not found because the `userid` is not provided with the `personalise` call, it will default to the `WRANDOM` assignment method detailed below.

#### WRANDOM
No attempt is made to ensure that A/B membership is 'sticky' over time. Instead, the system treats each call to the personalise service as a separate entity and assigns it in a weighted fashion to the `Abvariants`.
```python
from pyprimed.models.abvariant import ControlAbvariant, CustomAbvariant

e = mycampaign.experiments.create(name="myexperiment")
control = ControlAbvariant(label='A')
ab = CustomAbvariant(label='B', models={mymodel: 1.0})
e.abvariants.create({ab: 0.99, control: 0.01})
res1 = c.personalise(keys={'device': 'iphone', 'userid': '123'})
res2 = c.personalise(keys={'device': 'iphone'})
```
Regardless of `userid`, or `device` requests will be devided over the `Abvariant` and `CONTROL` in a 99%/1% ratio.

#### Excluding `targets` from `__CONTROL__`
If you want to exclude certain `targets` from `__CONTROL__` you can set the `exclude_from_control` property to `False` on `target`-creation:

```
# pyprimed
universe.targets.create(key="mykey", value="myvalue", exclude_from_control=True)
```